package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.YourDetailsPage;

/**
 * Created by nicklister on 13/03/2017.
 */
public class YourDetailsSteps {

    private YourDetailsPage ydPage = new YourDetailsPage();

    @Given("^I am on the partially registered prepay Your Details page$")
    public void i_am_on_the_partially_registered_prepay_Your_Details_page() throws Throwable {
        ydPage.loadPartialPrepayURL();
    }

    @When("^I enter billing details$")
    public void i_enter_billing_details() throws Throwable {
        ydPage.chooseBillingTitle();
        ydPage.typeInFirstNameField();
        ydPage.typeInLastNameField();
        ydPage.typeInPhoneField();
        ydPage.typeInAddressLookupPostcodeField();
        ydPage.addressLookupSubmit();
        ydPage.submit();
    }

    @When("^I enter account details$")
    public void i_enter_account_details() throws Throwable {
        ydPage.waitForAccountSection();
        ydPage.typeInConfirmEmailField();
        ydPage.typeInPasswordField();
        ydPage.typeInConfirmPasswordField();
    }

    @Then("^I land on the mocked submission page when I submit the form$")
    public void i_land_on_the_mocked_submission_page_when_I_submit_the_form() throws Throwable {
        ydPage.submit();
        ydPage.mockedSubmitPageIsShown();
    }

}
