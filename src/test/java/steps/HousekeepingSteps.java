package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import pages.BasePage;

/**
 * Created by nicklister on 13/03/2017.
 */
public class HousekeepingSteps {

    @Before
    public void open() {
        BasePage.openBrowser();
    }

    @After
    public void tearDown() {
        BasePage.closeBrowser();
    }
}
