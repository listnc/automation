package steps;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BasePage;
import pages.TOFPages;
import pages.TrolleyPage;
import pages.YourDetailsPage;

/**
 * Created by nicklister on 26/02/2017.
 */
public class TopOfFunnelSteps {

    private String homepageURL = "https://www.argos.co.uk";
    private TOFPages page = new TOFPages();
    private TrolleyPage trolley = new TrolleyPage();
    private YourDetailsPage ydPage = new YourDetailsPage();


    @Given("^a user is on the Argos homepage$")
    public void a_user_is_on_the_Argos_homepage() throws Throwable {
        page.loadURL(homepageURL);
    }

    @When("^they enter a search term \"([^\"]*)\"$")
    public void they_enter_a_search_term(String search) throws Throwable {
        page.search(search);
    }

    @Given("^a user adds product \"([^\"]*)\" to their trolley$")
    public void a_user_adds_product_to_their_trolley(String product) throws Throwable {
        page.loadProductURL(product);
        page.addToTrolley();
    }

    @When("^they select a product with SKU \"([^\"]*)\"$")
    public void they_select_a_product_with_SKU(String product) throws Throwable {
        page.clickProduct(product);
    }

    @When("^they navigate to the Home Delivery registration page$")
    public void they_navigate_to_the_Home_Delivery_registration_page() throws Throwable {
        trolley.loadTrolleyURL();
        trolley.localiseForHD();
        trolley.proceedToHDCheckout();
    }

    @When("^they complete the delivery section of the Your Details page$")
    public void they_complete_the_delivery_section_of_the_Your_Details_page() {
        ydPage.chooseDeliveryTitle();
    }

    @Then("^the URL which is shown is \"([^\"]*)\"$")
    public void the_URL_which_is_shown_is(String url) throws Throwable {
        page.validateURL(url);
    }





}
