package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by nicklister on 26/02/2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/", glue = "steps")

public class TestRunner {
}
