Feature:
  As a QA
  I want to test the UI part of the YD page
  So that I know it works

  Scenario: Complete partial Prepay registration
    Given I am on the partially registered prepay Your Details page
    When I enter billing details
    And I enter account details
    Then I land on the mocked submission page when I submit the form