Feature:
  As a user
  I want to load the Argos homepage
  So that I can search for a product

  Scenario: Navigate to PDP via browse
    Given a user is on the Argos homepage
    When they enter a search term "kettle"
    And they select a product with SKU "2371256"
    Then the URL which is shown is "http://www.argos.co.uk/product/2371256"

  Scenario: Navigate to new HD registration Your Details page
    Given a user adds product "9802759" to their trolley
    When they navigate to the Home Delivery registration page
    And they complete the delivery section of the Your Details page