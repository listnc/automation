package pages;

import org.junit.Assert;
import org.openqa.selenium.By;

/**
 * Created by nicklister on 26/02/2017.
 */
public class TOFPages extends BasePage {
    private String homepageURL = "https://www.argos.co.uk";
    private String productURL = "http://www.argos.co.uk/product/";

    private String searchField = "searchTerm";
    private String submitSearch = "[class='btn btn-search']";
    private String pdpAddToTrolleyButton = "[class='btn btn-primary add-to-trolley-cta']";
    private String addedToTrolleyOverlayQuantity = "add-to-trolley-product-quantity";


    public void loadProductURL(String product) {
        //driver.get(productURL + product);
        loadURL(productURL + product);
    }

    public void search(String search) {
        findElementByID(searchField).sendKeys(search);
        findElementByCSSSelector(submitSearch).click();
    }

    public void clickProduct(String product) {
        String productXpath = "//*[@name=" + product + "]//a";
        findElementByXPath(productXpath).click();
    }

    public void validateURL(String url) {
        Assert.assertTrue(currentURL().equals(url));
    }

    public void addToTrolley() {
        //findElementByXPath("//*[@class='btn btn-primary add-to-trolley-cta']").click();
        findElementByCSSSelector(pdpAddToTrolleyButton).click();
        waitForClass(addedToTrolleyOverlayQuantity);
        Assert.assertTrue(findElementByClassName(addedToTrolleyOverlayQuantity).isDisplayed());
    }
}
