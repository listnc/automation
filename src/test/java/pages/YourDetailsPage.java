package pages;

import gherkin.lexer.Th;
import org.junit.Assert;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by nicklister on 12/03/2017.
 */
public class YourDetailsPage extends BasePage {

    private String billingTitleField = "personTitle";
    private String firstNameField = "firstName";
    private String lastNameField = "lastName";
    private String phoneField = "phone";
    private String addressLookupPostcodeField = "lookupPostcode";
    private String addressLookupSubmit = "[class='btn btn-block btn-secondary']";

    private String billingAddressForm = "billingAddress";

    private String accountForm = "[class='panel panel-active'][data-el='panel_registration']";
    private String emailField = "email";
    private String confirmEmailField = "emailConfirm";
    private String passwordField = "password";
    private String confirmPasswordField = "passwordConfirm";

    private String submit = "[class='btn btn-block btn-primary']";
    private String mockedSuccessfulSubmissionPage = "submitMockPage";

    private String deliveryTitleField = "delivery_personTitle";
    private String partialPrepayURL = "http://localhost:3010/webapp/wcs/stores/servlet/AccessMyAccount";


    public void chooseDeliveryTitle() {
        Select deliveryTitle;

        deliveryTitle = new Select(findElementByID(deliveryTitleField));
        deliveryTitle.selectByValue("Mr");
    }

    public void loadPartialPrepayURL() {
        loadURL(partialPrepayURL);
        System.out.println(currentURL());
    }

    public void chooseBillingTitle() {
        Select billingTitle;

        billingTitle = new Select(findElementByID(billingTitleField));
        billingTitle.selectByValue("Mr");
    }

    public void typeInFirstNameField() {
        findElementByID(firstNameField).sendKeys("Nick");
    }

    public void typeInLastNameField() {
        findElementByID(lastNameField).sendKeys("Lister");
    }

    public void typeInPhoneField() {
        findElementByID(phoneField).sendKeys("07876543210");
    }

    public void typeInAddressLookupPostcodeField() {
        findElementByID(addressLookupPostcodeField).sendKeys("MK92NW");
    }

    public void addressLookupSubmit() throws InterruptedException {
        sleep(100);
        findElementByCSSSelector(addressLookupSubmit).click();
        waitForId(billingAddressForm);
    }

    public void submit() throws InterruptedException {
        sleep(100);
        findElementByCSSSelector(submit).click();
    }

    public void waitForAccountSection() {
        waitForCSS(accountForm);
    }

    public void clearEmailField() {
        findElementByID(emailField).clear();
    }

    public void typeInConfirmEmailField () {
        findElementByID(confirmEmailField).sendKeys("yourname@test.com");
    }

    public void typeInPasswordField() throws InterruptedException {
        sleep(100);
        findElementByID(passwordField).sendKeys("pa$$1234");
    }

    public void typeInConfirmPasswordField() throws InterruptedException {
        sleep(100);
        findElementByID(confirmPasswordField).sendKeys("pa$$1234");
    }

    public void mockedSubmitPageIsShown() {
        waitForId(mockedSuccessfulSubmissionPage);
        Assert.assertTrue(findElementByID(mockedSuccessfulSubmissionPage).isDisplayed());
    }
}
