package pages;

import org.junit.Assert;

/**
 * Created by nicklister on 12/03/2017.
 */
public class TrolleyPage extends BasePage {
    private String url = "https://www.argos.co.uk/webapp/wcs/stores/servlet/ArgosGSGiftModeExitCmd?action=OrderItemDisplay";
    private String emptyTrolleyIdentifier = "[id='emptytrolleylister']";
    private String trolleyIdentifier = "[class='wrapper-main-trolley']";

    private String postcodeSearch = "qasSearchTerm";
    private String unlocalisedDeliveryButton = "[data-el='main-trolley-deliver-button']";
    private String changeDeliveryPostcode = "[data-el='main-trolley-change-delivery-postcode-button']";
    private String continueToHDCheckoutButton = "[data-el='main-trolley-delivery-cta']";

    private String emailInput = "logonId";
    private String registerRadioButton = "registeruser";
    private String loginSubmit = "loginSubmit";

    public void loadTrolleyURL() {
        loadURL(url);
        waitForCSS(trolleyIdentifier);
        //Assert.assertTrue(findElementByCSSSelector(trolleyIdentifier).isDisplayed());
    }

    public void localiseForHD() {
        findElementByID(postcodeSearch).sendKeys("sg11xn");
        findElementByCSSSelector(unlocalisedDeliveryButton).click();
        waitForCSS(changeDeliveryPostcode);
    }

    public void proceedToHDCheckout() {
        findElementByCSSSelector(continueToHDCheckoutButton).click();
        waitForId(emailInput).sendKeys("country@bumpkin.org");
        findElementByID(registerRadioButton).click();
        findElementByID(loginSubmit).click();
    }


}
