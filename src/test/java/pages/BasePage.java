package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by nicklister on 11/03/2017.
 */
public class BasePage {
    protected static WebDriver driver;
    protected static WebDriverWait wait;

    public static void openBrowser() {
        /*static
        {*/

            driver = new FirefoxDriver();
            wait = new WebDriverWait(driver, 10);

        /*}*/
    }

    public static void closeBrowser() {
        driver.quit();
    }

    public void loadURL(String url) {
        driver.get(url);
    }

    public static void sleep(long ms) throws InterruptedException {
        Thread.sleep(ms);
    }

    public String currentURL() {
        String currentURL = driver.getCurrentUrl();
        return currentURL;
    }

    public WebElement findElementByID(String id) {
        return driver.findElement(By.id(id));
    }

    public WebElement findElementByClassName(String className) {
        return driver.findElement(By.className(className));
    }

    public WebElement findElementByCSSSelector(String cssSelector) {
        return driver.findElement(By.cssSelector(cssSelector));
    }

    public WebElement findElementByXPath(String xPath) {
        return driver.findElement(By.xpath(xPath));
    }

    public WebElement waitForClass(String locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
    }

    public WebElement waitForId(String locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
    }

    public WebElement waitForCSS(String locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
    }
}
